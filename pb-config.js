import {mobileOrTabletCheck, mobileCheck} from "./pb-functions.js?v=[cs_version]"


export var BASECONFIG = {}
BASECONFIG.DEFAULT_LOGIN_REDIRECT = "/"
BASECONFIG.JS_CACHE_ACTIVE = true
BASECONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 5000
BASECONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
BASECONFIG.DATEFORMAT = "DD.MM.YYYY"
BASECONFIG.DATETIMEWIHTOUTSECONDSFORMAT = "DD.MM.YYYY HH:mm"
BASECONFIG.DATETIMEFORMAT = "DD.MM.YYYY HH:mm:ss"
BASECONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
BASECONFIG.DB_DATEFORMAT_NOTIME = "YYYY-MM-DD"
BASECONFIG.DATEFORMAT_WEEKDAY = "ddd"
BASECONFIG.TIMEDISPLAYFORMAT = "HH:mm"
BASECONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "/csf-lib/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "/csf-lib/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
    {name: "socket.io", path: "/csf-lib/vendor/socket.io.js?v=[cs_version]", loaded: null},
    {name: "aes_js", path: "/csf-lib//vendor/aes.js?v=[cs_version]", loaded: null},
]
BASECONFIG.SOCKETIO_SUBPATH = "/socket"
BASECONFIG.NEWS = []
BASECONFIG.ERRORHANDLERS = ["handleAjaxErrors", "handleWindowErrors", "handleUnhandledRejections"]
BASECONFIG.ERRORREPORTING = {}
/* BASECONFIG.ERRORREPORTING.MODE = ["post"] */
BASECONFIG.ERRORREPORTING.MODE = []
BASECONFIG.ERRORREPORTING.POST_URL = undefined
BASECONFIG.ALWAYS_REDIRECT_TO_WWW = true
BASECONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR = 21
BASECONFIG.LANGUAGEFILE_PREFIX = undefined
BASECONFIG.HELPFILE_PREFIX = ""
BASECONFIG.EDITOR = "quill"
BASECONFIG.DOCUMENTATION = {}
BASECONFIG.DOCUMENTATION.EDITOR = "quill"
BASECONFIG.ISMOBILE = mobileCheck()
BASECONFIG.ISTABLETORMOBILE = mobileOrTabletCheck()
if (document.location.href.indexOf("http://localhost") == 0) {
    BASECONFIG.ALWAYS_REDIRECT_TO_WWW = false
    BASECONFIG.CIRCUIT_BASE_URL = "http://localhost:8080/circuits/"
    BASECONFIG.API_BASE_URL = "http://localhost:8000"
    BASECONFIG.APP_BASE_URL = "http://localhost:7070"
    BASECONFIG.SOCKET_SERVER = "http://localhost:8000"
    BASECONFIG.SOCKETIO_SUBPATH = "/socket"
    BASECONFIG.COOKIE_DOMAIN = "localhost"
    BASECONFIG.REGISTER_URL = "http://localhost:8080/register.html"
    if(BASECONFIG.ISMOBILE) {
        BASECONFIG.LOGIN_URL = "http://localhost:7070/login/persistent_login.html"
    }
} else {
    BASECONFIG.ERRORHANDLERS = []
    BASECONFIG.CIRCUIT_BASE_URL = "./circuits/"
    BASECONFIG.API_BASE_URL = "https://thor.planblick.com"
    BASECONFIG.APP_BASE_URL = "https://www.serviceblick.com"
    BASECONFIG.APP_STARTPAGE = "/"
    BASECONFIG.SOCKET_SERVER = "https://thor.planblick.com"
    BASECONFIG.COOKIE_DOMAIN = "www.serviceblick.com"
    BASECONFIG.LOGIN_URL = "https://www.serviceblick.com/login/"
    BASECONFIG.REGISTER_URL = "https://www.serviceblick.com/registration/"
    if(BASECONFIG.ISMOBILE) {
        BASECONFIG.LOGIN_URL = "https://www.serviceblick.com/login/persistent_login.html"
    }
}
