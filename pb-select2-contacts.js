import {CONFIG} from "../pb-config.js?v=[cs_version]"
import {getCookie, showNotification} from "./pb-functions.js?v=[cs_version]"


export class PbSelect2Contacts {
    constructor(selector) {
        this.selector = selector
        this.contacts = {}
    }

    setOnChange(onchange) {
        $(this.selector).on("change", (e)=>onchange(e))
        return this
    }

    getContactData(id) {
        console.log("HERE", id, this.contacts)
        if(this.contacts[id]) {
            return this.contacts[id]
        }
        else {
            return false
        }
    }

    setContactsDropdownFromApi(selectedValues = []) {
        let self = this
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/contacts/list",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (result) => {
                    self.setContactsDropdown(result, selectedValues)
                    resolve()
                },
                "error": () => showNotification("Oops something went wrong, please try again.")
            }

            $.ajax(settings)
        })
    }

    setContactsDropdown(values, selectedValues = []) {
        $(this.selector).html("")
        for (let value of values) {
            this.contacts[value.contact_id] = value
            let display_name = ""
            if((value.data.lastname && value.data.lastname.length > 0))
                display_name += `${value.data.lastname} `
            if((value.data.firstname && value.data.firstname.length > 0))
                display_name += `${value.data.firstname} `
            if((value.data.company && value.data.company.length > 0))
                display_name += `, ${value.data.company}`
            if(value.data.alias && value.data.alias.length > 0)
                display_name += `(${value.data.alias})`

            this.addContactToDropdown(display_name, value.contact_id)
        }
        $(this.selector).select2({
            tags: true,
            tokenSeparators: [',', ' '],
            dropdownParent: $(this.selector).parent()
        }).on('select2:open',function(){
            $('.select2-dropdown--above').attr('id','fix');
            $('#fix').removeClass('select2-dropdown--above');
            $('#fix').addClass('select2-dropdown--below');
        })
        this.setSelectedValues(selectedValues)
        return this
    }

    addContactToDropdown(name, value) {
        if ($(`${this.selector} option[value="${value}"]`).length === 0) {
            $(this.selector).append($('<option>', {
                value: value,
                text: name
            })).trigger('change')
        }

        return this
    }

    setSelectedValues(selectedValues) {
        for (let tag of selectedValues) {
            if (tag.length > 0 && $(`${this.selector} option[value="${tag}"]`).length === 0) {
                this.addContactToDropdown(tag, tag)
            }
        }
        $(this.selector).val(selectedValues).trigger('change')
        return this
    }

    getSelectedValues() {
        return $(this.selector).val()
    }
}