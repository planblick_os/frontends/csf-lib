import {CONFIG} from "../../../pb-config.js?v=[cs_version]"
import {getCookie} from "../pb-functions.js?v=[cs_version]"

export class PbApiRequest {
    constructor(path, type = "GET", payload = {}, apikey = getCookie("apikey"), config = CONFIG) {
        this.path = path
        this.type = type
        this.config = config
        this.payload = payload
        this.settings = {}
        this.apikey = apikey
    }

    execute() {
        let self = this
        this.createSettings()

        return new Promise((resolve, reject) => {
            this.settings["success"] = (response) => resolve(response)
            this.settings["error"] = (result) => reject(result)
            $.ajax(this.settings)
        })
    }

    createSettings() {
        let self = this
        switch (this.type) {
            case "GET":
                this.settings = {
                    "url": self.config.API_BASE_URL + this.path,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": this.apikey
                    }
                }
                break;
            case "POST":
                this.settings = {
                    "url": self.config.API_BASE_URL + this.path,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/json",
                        "apikey": this.apikey
                    },
                    "data": JSON.stringify(this.payload),
                }
                break;
            default:
                this.settings = {}
                throw "REQUEST TYPE UNKNOWN:" + this.type
                break
        }
    }
}
