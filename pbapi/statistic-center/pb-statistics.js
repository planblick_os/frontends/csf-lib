import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"
import {getCookie, makeId} from "/csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"

export class PbStatistics {
    static baseconfig = {
        config: {apikey: getCookie("apikey"), api_base_url: CONFIG.API_BASE_URL},
        onBackendUpdateFunc: () => "",
        onCommandError: () => "",
        onEventError: () => "",
    }


    constructor(args = PbStatistics.baseconfig) {
        this.functionList = [];
        this.config = args.config
        this.onBackendUpdateFunc = args.onBackendUpdateFunc
        this.onCommandError = args.onCommandError
        this.onEventError = args.onEventError
        this.allConfigs = undefined;
        return this
    }

    fetchFunctionList() {
        return new Promise(async (resolve, reject) => {
            new PbApiRequest("/statistics/functions/list").execute().then(function (response) {
                this.functionList = response;
                resolve(response);
            }.bind(this));
        });
    };

    putFunction(payload) {
        return new Promise(async (resolve, reject) => {
            new PbApiRequest("/es/cmd/putStatisticFunction", 'POST', payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    }

    deleteFunction(id, version) {
        return new Promise(async (resolve, reject) => {
            let payload = {
                "id": id,
                "version": version
            };

            new PbApiRequest("/es/cmd/deleteStatisticFunction", 'POST', payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    };

    simulateLambdaFunction(code) {
        return new Promise(async (resolve, reject) => {
            let payload = {
                "code": code
            };

            new PbApiRequest("/statistics/functions/execute_direct", 'POST', payload).execute().then(function (response) {
                resolve(response);
            }.bind(this));
        });
    }
}
