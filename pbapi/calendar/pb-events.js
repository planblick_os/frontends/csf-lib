import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"
import {getCookie, resolvePath, setCookie, setPath} from "../../pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"

export class PbEvents {
    #data = []
    #activeEvent = {
        "event_id": "",
        "resourceId": "",
        "textColor": "",
        "color": "",
        "attachedRessources": [],
        "starttime": "",
        "endtime": "",
        "title": "",
        "savekind": "create",
        "extended_props": {
            "creator_login": ""
        },
        "data": {
            "event_status": "confirmed"
        },
        "allDay": false,
        "data_owners": [],
        "attachedContacts": [],
        "reminders": [],
        "tags": []
    }
    #lastTaskId = undefined

    constructor(socket, onBackendUpdateFunc = () => "", onBackendDeleteFunc = () => "") {
        this.socket = socket
        this.onBackendUpdateFunc = onBackendUpdateFunc
        this.onBackendDeleteFunc = onBackendDeleteFunc
        this.initBackendListeners()

    }

    loadFromApi(startDate = "2021-09-01T00:00:00", endDate = "2225-09-30T00:00:00") {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest(`/cb/events?start=${startDate}&end=${endDate}`).execute().then((response) => {
                self.#data = []
                for (let row of response) {
                    row.attachedRessources = row.attachedRessources.map(res => JSON.parse(res))
                    self.#data.push(row)
                }
                resolve(self)
            })
        })
    }

    getEventDataFromApi(event_id) {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest(`/cb/event/${event_id}`).execute().then((response) => {
                self.#activeEvent = response
                resolve(self)
            })
        })
    }

    getEventList(splitUpMultiDayEvents = false) {
        let retVal = []
        const events = this.#data
        if (splitUpMultiDayEvents === true) {
            for (let event of events) {
                let startdate = moment(event.start, CONFIG.DB_DATEFORMAT)
                let enddate = moment(event.end, CONFIG.DB_DATEFORMAT)

                if (!startdate.isSame(enddate, 'date')) {
                    if (enddate.isBefore(startdate)) {
                        let tmp = enddate
                        enddate = startdate
                        startdate = tmp
                    }
                    // Add first day to results
                    let new_event = Object.assign({}, event)
                    new_event.end = startdate.endOf('day').format(CONFIG.DB_DATEFORMAT)
                    retVal.push(new_event)

                    // Add modified copies of the event until enddate is reached
                    while (!startdate.isSame(enddate, 'date')) {
                        let new_event = Object.assign({}, event)
                        startdate.add(1, 'days').startOf('day')
                        new_event.start = startdate.format(CONFIG.DB_DATEFORMAT)
                        new_event.end = startdate.endOf('day').format(CONFIG.DB_DATEFORMAT)
                        retVal.push(new_event)
                    }
                    //We have to fix the endtime which is set to endOfDay by while so pop the last entry...
                    retVal.pop()

                    // and re-add last day with fixed starttime but unmodified endtime
                    enddate.startOf('day')
                    new_event = Object.assign({}, event)
                    new_event.start = enddate.format(CONFIG.DB_DATEFORMAT)
                    retVal.push(new_event)

                } else {
                    retVal.push(event)
                }
            }
        } else {
            retVal = this.#data
        }

        return retVal
    }

    getActiveEventData() {
        return this.#activeEvent
    }

    setActiveEventData(newEventData) {
        this.#activeEvent = newEventData;
    }

    updateActiveEvent(event_id, skipOnBackendUpdateFunc = false) {
        let self = this
        this.getEventDataFromApi(event_id).then(() => {
            this.onBackendUpdateFunc(skipOnBackendUpdateFunc)
        })
    }

    deleteEvent(event_id) {
        return new Promise((resolve, reject) => {
            new PbApiRequest(`/es/cmd/deleteAppointment`, "POST", {"event_id": event_id}).execute().then((response) => {
                resolve(self)
            })
        })
    }

    initBackendListeners() {
        if (this.socket) {
            let self = this
            this.socket.commandhandlers["addEvent"] = function (args) {
                if (self.#lastTaskId) {
                    self.#lastTaskId = undefined
                    self.updateActiveEvent(args.id, true)
                } else {
                    self.updateActiveEvent(args.id, false)
                }
            }

            this.socket.commandhandlers["removeEvent"] = function (args) {
                if (self.#lastTaskId) {
                    self.#lastTaskId = undefined
                    self.onBackendDeleteFunc()
                } else {
                    self.onBackendDeleteFunc()
                }
            }
        }
    }

    saveActiveEvent(data) {
        let self = this
        return new Promise((resolve, reject) => {
            for (let [prop, val] of Object.entries(data)) {
                let fieldExists = resolvePath(this.#activeEvent, prop, undefined)
                setPath(this.#activeEvent, prop, val)
            }
            setPath(this.#activeEvent, "extended_props.creator_login", getCookie("username"))
            if (this.#activeEvent.event_id != "") {
                new PbApiRequest(`/es/cmd/changeAppointment`, "POST", this.#activeEvent).execute().then((response) => {
                    self.#lastTaskId = response.task_id
                    resolve()
                })
            } else {
                setCookie("data_owners", JSON.stringify(this.#activeEvent.data_owners), {expires: 3650, path: '/'})
                setCookie("last_event_types_selection", JSON.stringify(this.#activeEvent.data.appointment_type), {expires: 3650, path: '/'})
                setCookie("last_event_status_selection", this.#activeEvent.data.event_status, {expires: 3650, path: '/'})
                setCookie("last_event_members_selection", JSON.stringify(this.#activeEvent.attachedRessources), {expires: 3650, path: '/'})
                setCookie("last_event_tag_selection", JSON.stringify(this.#activeEvent.tags), {expires: 3650, path: '/'})
                new PbApiRequest(`/es/cmd/createNewAppointment`, "POST", this.#activeEvent).execute().then((response) => {
                    self.#lastTaskId = response.task_id
                    resolve()
                })
            }
        })
    }

    formatDateTime(dateString) {
       return moment(dateString).format(CONFIG.DB_DATEFORMAT);
    }
}
