import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbEventStatus {
    #status_list = []

    constructor(socket, onBackendUpdateFunc = () => "") {
        this.onBackendUpdateFunc = onBackendUpdateFunc
    }

    load() {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest("/cb/geteventstatus").execute().then((response) => {
                self.#status_list = []
                if (!response || response.length === 0) {
                    self.#status_list.push({
                            "color": "#FD7E14",
                            "default": false,
                            "status_id": "pending",
                            "status_name": "pending"
                        },
                        {
                            "color": "#FFC107",
                            "default": false,
                            "status_id": "reserved",
                            "status_name": "reserved"
                        },
                        {
                            "color": "#3BB001",
                            "default": false,
                            "status_id": "confirmed",
                            "status_name": "confirmed"
                        },
                        {
                            "color": "#3366FF",
                            "default": false,
                            "status_id": "canceled",
                            "status_name": "canceled"
                        },
                        {
                            "color": "#7987A1",
                            "default": false,
                            "status_id": "no-show",
                            "status_name": "no-show"
                        })
                } else {
                    for(let status_entry of response) {
                        self.#status_list.push({
                            "color": status_entry.data.color,
                            "default": status_entry.data.default,
                            "status_id": status_entry.data.status_id,
                            "status_name": status_entry.data.status_name
                        })
                    }
                }
                resolve(self)
            })
        })
    }

    async set(payload) {
        return new Promise((resolve, reject) => {
            new PbApiRequest("/es/cmd/putAppointmentStatus", "POST", payload).execute().then(() => {
                resolve()
            }).catch(()=>reject)
        })
    }

    getStatusColors() {
        return this.statusColors
    }

    getStatusList() {
        return this.#status_list
    }

    getSelectOptions(selectedIds = []) {
        let status_options = []
        for (let entry of this.#status_list) {
            let option = {
                "id": entry.status_id,
                "name": entry.status_name,
                "selected": selectedIds.includes(entry.status_id) ? "selected" : ""
            }
            status_options.push(option)
        }
        return status_options
    }

    async deleteByStatusId(status_id) {
        return new Promise(function (resolve, reject) {
            let payload = {
                "data": {
                    "status_id": status_id
                }
            }
            new PbApiRequest("/es/cmd/removeAppointmentStatus", "POST", payload).execute().then(async function (response) {
                resolve()
            }.bind(this))
        }.bind(this))
    }
}
