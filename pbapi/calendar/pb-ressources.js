import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbRessources {
    #data

    constructor(socket, onBackendUpdateFunc = () => "") {
        this.#data = {}
        this.onBackendUpdateFunc = onBackendUpdateFunc
        this.socket = socket
    }

    loadFromApi() {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest("/cb/ressources").execute().then((response) => {
                for (let res of response) {
                    self.#data[res.ressource_id] = res
                }
                resolve(self)
            })
        })
    }

    getData() {
        return this.#data
    }

    update() {
        this.onBackendUpdateFunc()
    }

    getSelectOptions(selectedIds = []) {
        let options = []

        for (let [key, entry] of Object.entries(this.#data)) {
            let id_value = {
                "id": entry.id,
                "name": entry.display_name,
                "fontColor": entry.props.eventFontColor,
                "backgroundColor": entry.props.eventBackgroundColor,
                "loginId": entry.props.loginId,
                "ressource_id": entry.ressource_id
            }

            let option = {
                "id": JSON.stringify(id_value),
                "name": entry.display_name,
                "selected": selectedIds.includes(JSON.stringify(id_value)) ? "selected" : ""
            }
            options.push(option)
        }
        return options
    }
}
