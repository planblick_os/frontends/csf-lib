import { PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbColumns {
    #data = {}
    constructor() {

    }

    loadFromApi() {
        let self = this
        return new Promise((resolve, reject) => {
            new PbApiRequest("/cb/columns").execute().then((response)=>{
                self.#data = response
                resolve(self)
            })
        })
    }

    getColumnList() {
        return this.#data
    }

    getSelectOptions(selectedIds= []) {
        let columns = Object.values(this.#data).flat(1)
        let columns_options = []
        for (let entry of columns) {
            if (entry.id == "others") {
                continue
            }
            let option = {
                "id": entry.id,
                "name": entry.title,
                "selected": selectedIds.includes(entry.id) ? "selected" : ""
            }
            columns_options.push(option)
        }
        columns_options = Object.values(
            columns_options.reduce((col, obj) => ({...col, [obj.id]: obj}), {})
        )
        return columns_options
    }
}
