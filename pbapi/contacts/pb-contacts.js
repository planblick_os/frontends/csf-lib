import { PbApiRequest} from "../pb-api-request.js?v=[cs_version]"
import { ContactManager } from "../../../contact-manager/js/contact-manager.js?v=[cs_version]";

export class PbContacts {
    #data = {}
    #fieldconfig = undefined
    constructor() {

    }

    loadFromApi() {
        let self = this
        return new Promise(async (resolve, reject) => {
            await new PbApiRequest("/contacts/fieldconfig").execute().then((response) => {
                self.#fieldconfig = response
            })
            await new PbApiRequest("/contacts/list").execute().then((response) => {
                console.log("FIELDCONFIG", self.#fieldconfig)
                self.#data = response
                //find out from the config settings, which details are to be shown in the list view
                let tmp_titles = {}
                let tmp_subtitles = {}

                for (let configEntry of self.#fieldconfig?.data) {
                    if (configEntry.display_pos_in_list_h1 > 0) {
                        tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
                    }
                    if (configEntry.display_pos_in_list_h2 > 0) {
                        tmp_subtitles[configEntry.display_pos_in_list_h2] = configEntry.field_id
                    }
                }

                self.#data.forEach(entry => {
                    entry.data.displayTitle = this.formatContactTitle(entry, tmp_titles);
                    entry.data.displaySubtitle = this.formatContactSubtitle(entry, tmp_subtitles);
                });
            })

            resolve(self)
        })

    }

    formatContactTitle(configEntry, tmp_titles) {
        let contactTitleString = ""

        for (let titlefield_key of Object.keys(tmp_titles)) {
            if (configEntry.data[tmp_titles[titlefield_key]]) {
                contactTitleString += configEntry.data[tmp_titles[titlefield_key]] + " "
            }
        }

        return contactTitleString
    }

    formatContactSubtitle(configEntry, tmp_subtitles) {
        let contactSubtitleString = ""

        for (let titlefield_key of Object.keys(tmp_subtitles)) {
            if (configEntry.data[tmp_subtitles[titlefield_key]]) {
                contactSubtitleString += configEntry.data[tmp_subtitles[titlefield_key]] + " "
            }
        }

        return contactSubtitleString
    }

    getData() {
        return this.#data
    }

    getSelectOptions(selectedIds= []) {
        let contact_options = []

        for (let entry of this.#data) {
            if (entry.id == "others") {
                continue
            }
            let option = {
                "id": entry.contact_id,
                "name": entry.data.displayTitle ? entry.data.displayTitle : entry.data.firstname + " " + entry.data.lastname,
                "email": entry.data.email,
                "selected": selectedIds.includes(entry.contact_id) ? "selected" : ""
            }
            contact_options.push(option)
        }
        return contact_options
    }
}
