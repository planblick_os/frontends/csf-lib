import {fireEvent, getCookie} from "./pb-functions.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {validateForm,fadeIn,clearForm} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"




document.addEventListener('logoutButtonClicked', function() {new PbAccount().logout()})

$("#logout_button").click(function (event) {
    fireEvent("logoutButtonClicked", {
        "login": getCookie("username"),
        "consumer": getCookie("consumer_name")
    })
})


document.addEventListener('showLogoutNotification', function() {alert("logout") })


$(document).on('click', "[data-listener='password_change_click']", function (e) {
    e.preventDefault();

    if (!new PbAccount().hasPermission("dashboard.management.change_own_password")) {
        showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum ändern ihres Passworts."))
    } else {
        $.blockUI({message: $('#password_change_modal')})
        
    }

})

$(document).on('click', '#submit_password_dialog_button', function (e) {
    e.preventDefault()

    let func = function (apikey) {
        if (validateForm({ 'old_password': 'input' }) == false || apikey == false) {
            console.log("validate api key")
            
            document.getElementById('login-error-msg').innerHTML = '<div class="alert alert-danger error_message">*Das angegebene Passwort stimmt nicht. Falls Du es vergessen haben solltest, kannst du es <a class="tx-danger" href="/login/passwort.html">hier</a> zurücksetzen *</div>'
            fadeIn("login-error-msg")

        } else if (validateForm({ 'new_password': 'password' }) == false) {
           
            document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-danger error_message'>*Das Passwort muss mindestens 8 Zeichen ohne Lehrzeichen, mit mindestens 1 Groß-, 1 Kleinbuchstaben, 1 Ziffer und 1 Sonderzeichen enthalten.*</div>"
            fadeIn("login-error-msg")

        } else if (validateForm({ 'new_password_confirm': 'input' }) == false || $("#new_password").val() != $("#new_password_confirm").val()) {
   
            document.getElementById('login-error-msg').innerHTML = "<div class='alert alert-danger error_message'>*Das neues Passwort und das Passwort-Bestätigung stimmen nicht überein.*</div>"
            fadeIn("login-error-msg")

        } else {
            
            new PbAccount().changePassword($.cookie("username"), $("#new_password").val());
            clearForm('old_password', 'new_password', 'new_password_confirm')
            document.getElementById('login-error-msg').innerHTML = ""
        }
    }
    
    new PbAccount().getapikeyfor($.cookie("username"), $("#old_password").val(), func)



})