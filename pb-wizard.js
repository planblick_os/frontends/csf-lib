import {DtWizardScenario, DtWizardStep} from "./pb-wizard-datatypes.js"

export class Wizard {
    #scenario = {}
    #currentStepNumber = undefined
    constructor(pbtpl) {
        this.pbtpl = pbtpl
    }

    newScenario(name, steps) {
        this.#scenario = new DtWizardScenario(name, steps)
        return this
    }

    renderStep(stepNumber, render_data = {}) {
        this.#currentStepNumber = stepNumber
        let currentStep = this.#scenario.getStep(this.#currentStepNumber)
        this.pbtpl.renderIntoAsync(currentStep.bodytemplate, render_data, "#step_bodycontent").then(()=>currentStep.onload(currentStep))
    }

    renderStepOverviewContent() {
        this.pbtpl.renderIntoAsync("csf-lib/pb/wizard/templates/step_header.tpl", this.#scenario.steps, "#wizard_steps_overview_container", "append")
    }

    getScenario() {
        return this.#scenario
    }

    addStep(step_number, title, subtitle, bodytemplate, onload, onleave) {
        this.#scenario.addStep(new DtWizardStep(step_number, title, subtitle, bodytemplate, onload, onleave))
    }

    next() {
        let prevStepNumber = this.#currentStepNumber
        let prevStep = this.getScenario().getStep(prevStepNumber)
        if(prevStep.onleave(prevStep) !== false) {
            this.renderStep(this.#currentStepNumber + 1)
        }

    }

    previous() {
        let prevStepNumber = this.#currentStepNumber
        this.renderStep(this.#currentStepNumber - 1)
        let prevStep = this.getScenario().getStep(prevStepNumber)
        prevStep.onleave(prevStep)
    }

    initListeners() {

    }
}
//# sourceMappingURL=pb-wizard.js.map