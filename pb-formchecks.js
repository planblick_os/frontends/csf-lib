export function validateForm(params, funcs = {}, showErrors = true) {

    //alert("I am in validate form now")
    let result = {}
    let return_value = true

    for (let [elem_id, type] of Object.entries(params)) {
        if (type == "textarea") {
            result[elem_id] = validateField(elem_id)
        } else if (type == "input") {
            result[elem_id] = validateField(elem_id)
        } else if (type == "email") {
            result[elem_id] = validateField(elem_id) && isEmail($("#"+elem_id).val())
        } else if (type == "password") {
            result[elem_id] = isStrongPwd($("#"+elem_id).val())
        }

        if (funcs[type]) {
            result[elem_id] = funcs[type](elem_id)
        }

        for (let [elem_id, check_result] of Object.entries(result)) {
            if (check_result == false) {
                return_value = false
            }
        }
    }

    if (showErrors) {
        handleGUIonFormError(result)
    } else {
        return result
    }
    return return_value
}

export function validateField(elemId) {
    let fieldWithId = document.getElementById(elemId)
    let checkResult = fieldWithId.checkValidity()
    return checkResult
}

export function handleGUIonFormError(checkResults) {
    for (let [elem_id, result] of Object.entries(checkResults)) {
        let fieldName = elem_id
        let fieldWithId = document.getElementById(fieldName)
        let errorId = document.getElementById(fieldName + "Error")
        if (result == false) {
            errorId.classList.replace("error", "error-visible")
            fieldWithId.classList.add("invalid")
            errorId.setAttribute("aria-hidden", false)
            errorId.setAttribute("aria-invalid", true)
        } else {
            errorId.classList.replace("error-visible", "error")
            fieldWithId.classList.remove("invalid")
            errorId.setAttribute("aria-hidden", true)
            errorId.setAttribute("aria-invalid", false)
        }
    }
}

export function handleGUIbyidError(elem_id, result) {
    let fieldName = elem_id
    let fieldWithId = document.getElementById(fieldName)
    let errorId = document.getElementById(fieldName + "Error")
    if(errorId) {
        if (result == false) {
            errorId.classList.replace("error", "error-visible")
            fieldWithId.classList.add("invalid")
            errorId.setAttribute("aria-hidden", false)
            errorId.setAttribute("aria-invalid", true)
        } else {
            errorId.classList.replace("error-visible", "error")
            fieldWithId.classList.remove("invalid")
            errorId.setAttribute("aria-hidden", true)
            errorId.setAttribute("aria-invalid", false)
        }
    }
}


export function validateLetNumbUnderField(elemId) {
    let fieldWithId = document.getElementById(elemId)
    if (fieldWithId.value == "") {
        //console.log("Error: Username cannot be blank!")
        return false
    }
    let re = /^\w+$/;
    if (!re.test(fieldWithId.value)) {
        //console.log("Error: Username must contain only letters, numbers and underscores!")
        return false
    }
    return true
}

export function isStrongPwd(password) {

    var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var lowercase = "abcdefghijklmnopqrstuvwxyz";
    var digits = "0123456789";
    var splChars = "!@#$%^&*()\\-__+.";
    var ucaseFlag = contains(password, uppercase);
    var lcaseFlag = contains(password, lowercase);
    var digitsFlag = contains(password, digits);
    var splCharsFlag = contains(password, splChars);

    if (password.length >= 8 && ucaseFlag && lcaseFlag && digitsFlag && splCharsFlag)
        return true;
    else
        return false;

}

export function contains(password, allowedChars) {
    for (let i = 0; i < password.length; i++) {
        let char = password.charAt(i);
        if (allowedChars.indexOf(char) >= 0) {
            return true;
        }
    }
    return false;
}

export function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


export function fadeIn(element_id) {
    //element_id = "error-msg"
    var fade = document.getElementById(element_id)
    var opacity = 0;
    var intervalID = setInterval(function () {
        if (opacity < 1) {
            opacity = opacity + 0.5
            fade.style.opacity = opacity;

        } else {
            clearInterval(intervalID);
        }
    }, 200);
}

export function clearForm() {
    for (var i = 0, j = arguments.length; i < j; i++) {

        var field = document.getElementById(arguments[i])
        field.value = ""
    }
}

export function stepValidatePassword(formInputId) {
    var myInput = $("#" + formInputId);
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var special = document.getElementById("special");
    var length = document.getElementById("length");
    
    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    var numbers = /[0-9]/g;
    var specials = /[!@#$%^&*()\-__+.]/g;
    
    // When the user clicks on the password field, show the message box
    myInput.on( "focus", function() {
        document.getElementById("message").style.display = "block";
      } )
 
    // When the user clicks outside of the password field, hide the message box
    myInput.on( "blur", function() {
      document.getElementById("message").style.display = "none";
    })
    
    // When the user starts to type something inside the password field
    myInput.on( "keyup",  function() {
    // Validate lowercase letters
    myInput.val().match(lowerCaseLetters) ? gotMatch(letter) : goNoMatch(letter)
    // Validate capital letters
    myInput.val().match(upperCaseLetters) ? gotMatch(capital) : goNoMatch(capital)
    // Validate numbers
    myInput.val().match(numbers) ? gotMatch(number) : goNoMatch(number)
    // Validate length
    myInput.val().length >= 8 ? gotMatch(length) : goNoMatch(length)
    // Validate special xharacters
    myInput.val().match(specials) ? gotMatch(special) : goNoMatch(special)
    
    function gotMatch(x) { 
            x.classList.remove("invalid-password");
            x.classList.add("valid-password");
        }
        
    function goNoMatch(x) {
            x.classList.remove("valid-password");
            x.classList.add("invalid-password");
        } 
    
    })

}

