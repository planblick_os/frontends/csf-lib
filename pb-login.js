import {fireEvent, getCookie, makeId, showNotification} from "./pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class PbLogin {
    profileData = {}

    constructor() {
        if (!PbLogin.instance) {
            PbLogin.instance = this
            PbLogin.instance.initListeners()
        }

        return PbLogin.instance
    }

    init(login) {
        return new Promise(function (resolve, reject) {
            PbLogin.instance.fetchProfileData(login).then(
                function (response) {
                    PbLogin.instance.setProfileData(response)
                    resolve(PbLogin.instance)
                },
                reject
            )
        })
    }

    resetPassword(login, password=null) {
        let url = CONFIG.API_BASE_URL + "/es/cmd/resetLoginPassword"
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": url,
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "username": login,
                    "password": password != null ? password : makeId(15)
                }),
                "success": function (response) {
                    resolve(response)
                },
                "error": function (error) {
                    reject(error)
                }
            }

            $.ajax(settings)
        })
    }

    fetchProfileData(login) {
        let url = CONFIG.API_BASE_URL + "/get_login_profile?login=" + login

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": url,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "error": reject
            };

            $.ajax(settings).done(function (response) {
                resolve(response)
            })
        })
    }

    setProfileData(data) {
        PbLogin.instance.profileData = data
    }

    saveProfileData() {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateLoginProfileData",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({data: PbLogin.instance.profileData}),
                "error": reject
            }

            $.ajax(settings).done(function (response) {
                resolve(response)
            })
        })
    }

    initListeners() {

    }
}