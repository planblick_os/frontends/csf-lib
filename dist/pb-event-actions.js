import {PbBaseEventActions} from "./csf-lib/pb-base-event-actions.js?v=[cs_version]"
import {PbAccount} from "./csf-lib/pb-account.js?v=[cs_version]";
import {LoginModule} from "./csf-load-module.js?v=[cs_version]";

export class PbEventActions extends PbBaseEventActions {
    constructor() {
        super();
        this.initActions()
        if (!PbEventActions.instance) {
            PbEventActions.instance = this;
        }

        return PbEventActions.instance;
    }

    initActions() {
        this.actions["init"] = function (myevent, callback = () => "") {

        }

        this.actions["initModuleLogin"] = function (myevent, callback = () => "") {
            let actions = new LoginModule().actions
            PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
            callback(myevent)
        }

        this.actions["initModuleRegistration"] = function (myevent, callback = () => "") {
            let actions = new RegistrationModule().actions
            PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
            callback(myevent)
        }
        this.actions["initModuleDashboard"] = function (myevent, callback = () => "") {
            let actions = new DashboardModule().actions
            PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
            callback(myevent)
        }
        this.actions["initModuleE2OForm"] = function (myevent, callback = () => "") {
            let actions = new E2OFormModule().actions
            PbEventActions.instance.actions = Object.assign(PbEventActions.instance.actions, actions);
            callback(myevent)
        }

        this.actions["logoutButtonClicked"] = function (myevent, callback = () => "") {
            new PbAccount().logout()
            callback(myevent)
        }
    }
}


