import {BASECONFIG} from "./csf-lib/pb-config.js?v=[cs_version]";

export const CONFIG = BASECONFIG

CONFIG.IMPORT_BASEPATH="/wp-content/plugins/crowdsoft-connector/"

CONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "/wp-content/plugins/crowdsoft-connector/public/js/csf-lib/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "/wp-content/plugins/crowdsoft-connector/public/js/csf-lib/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
    {name: "socket.io", path: "/wp-content/plugins/crowdsoft-connector/public/js/csf-lib/vendor/socket.io.js?v=[cs_version]", loaded: null},
]

if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    CONFIG.CIRCUIT_BASE_URL = "http://localhost:8080/wp-content/plugins/crowdsoft-connector/public/circuits/"
    CONFIG.APP_BASE_URL = "http://localhost:7000"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.SOCKET_SERVER = "https://thor.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"

} else if (document.location.href.indexOf("http://fk.planblick.com") == 0) {
    console.log("Using DEV-Config")
    CONFIG.CIRCUIT_BASE_URL = "http://fk.planblick.com:7000/circuits/"
    CONFIG.APP_BASE_URL = "http://fk.planblick.com:7000"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.SOCKET_SERVER = "https://thor.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"

} else if (document.location.href.indexOf("testblick.de") != -1) {
    console.log("Using STAGING-Config")
    CONFIG.CIRCUIT_BASE_URL = "//testblick.de/circuits/"
    CONFIG.APP_BASE_URL = "//testblick.de"
    CONFIG.API_BASE_URL = "//odin.planblick.com"
    CONFIG.LOGIN_URL = "//testblick.de/login/"
    CONFIG.SOCKET_SERVER = "//odin.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"

} else {
    console.log("Using LIVE-Config")
    CONFIG.CIRCUIT_BASE_URL = "https://www.serviceblick.com/circuits/"
    CONFIG.APP_BASE_URL = "https://www.serviceblick.com"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.LOGIN_URL = "https://www.serviceblick.com/login/"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
}



