import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {getCookie} from "./pb-functions.js?v=[cs_version]";

export class SagaStep {
    constructor() {
        this.name
        this.event_name
        this.order
        this.payload = {}
    }

    getJSON() {
        let json = {
            "step_name": this.name,
            "event_name": this.event_name,
            "order": this.order,
            "payload": this.payload
        }
        return JSON.stringify(json)
    }

    setName(name_value) {
        this.name = name_value
    }

    setEventName(event_value) {
        this.event_name = event_value
    }

    setOrder(num_value) {
        this.order = num_value
    }

    setPayload(data, options) {
        this.payload["data"] = data
        this.payload["data_owners"] = options?.data_owners || []
        this.payload["parent_ids"] = options?.parent_ids || []
        this.payload["child_ids"] = options?.child_ids || []
        this.payload["external_id"] = options?.external_id || undefined
    }
}

export class Saga {
    constructor(socket = undefined, newSagaCreatedCallback = () => {
    }) {
        this.task_id
        this.socket = socket
        this.name
        this.id
        this.steps = []
        this.newSagaCreatedCallback = newSagaCreatedCallback
        this.initListeners()
        //this.current_step
    }

    create(newSagaCreatedCallback = undefined) {
        this.newSagaCreatedCallback = newSagaCreatedCallback || this.newSagaCreatedCallback
        let self = this
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewSaga",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "success": response => {
                    self.setTaskId(response.task_id)
                    resolve()
                },
                "data": JSON.stringify({
                    "saga_name": self.name,
                    "saga_data": self.steps,
                    "data_owners": []
                }),
                "error": () => {
                }
            };


            $.ajax(settings)
        })
    }


    setSagaName(name) {
        this.name = name
    }

    getStepNames() {
        const step_names = this.steps.map(step => step.step_name)
        return step_names
    }

    setTaskId(id) {
        this.task_id = id
        return this
    }

    setSagaId(id) {
        this.id = id
        return this
    }

    addStep(step) {
        if(typeof step == "object") {
            step = JSON.parse(step.getJSON())
        }
        this.steps.push(step)
        return this
    }

    nextStep() {
        let self = this
        return new Promise(function (resolve, reject) {
            debugger
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/saga/next/correlation_id/" + self?.task_id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => {resolve(response)},
                "error": reject
            }

            $.ajax(settings)
        })

    }

    fastForward(until_step = undefined) {
        let self = this
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + `/es/saga/fast_forward/correlation_id/${self.task_id}/step/${until_step}`,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => resolve(() => console.log("Fast-Forward", response)),
                "error": reject
            };

            $.ajax(settings)
        })


    }

    getStepByIndex(index) {
        return this.steps[index]
    }

    initListeners() {
        let self = this
        if (this.socket) {
            this.socket.commandhandlers["newSagaCreated"] = function (args) {
                console.log("recent_task_id", args.task_id)
                if (self.task_id == args.task_id) {
                    self.setSagaId(args.saga_id)
                    self.newSagaCreatedCallback(args)
                }
            }

            this.socket.commandhandlers["showTaskError"] = function (args) {
                alert("Fehler:" + args.text)
            }
        }
    }

    getSagaPayload() {
        let saga_payload = {
            'saga_name': this.name,
            'saga_data': this.steps
        }
        return saga_payload
    }

    //getCurrentStep() {}

}





