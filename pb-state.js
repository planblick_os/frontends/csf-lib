import {md5} from '../../csf-lib/pb-functions.js?v=[cs_version]'

export class PbState {
    constructor() {
        if (!PbState.instance) {
            PbState.instance = this
            PbState.instance.currentStateIndex = 0
            PbState.instance.states = []
            window.onpopstate = function (event) {
                if(event.state == null) {
                    return
                }
                let previousStateIndex = event.state.currentIndex ? event.state.currentIndex : 0
                // // add "push_state"=true to args
                let state = PbState.instance.states[previousStateIndex]
                state.args.push(false)
                state.func(...state.args)
                PbState.instance.currentStateIndex = previousStateIndex
            }
        }
        return PbState.instance
    }

    pushStateFunction(func, ...args) {
        let funcArgsHash = md5(func.toString() + JSON.stringify(args))
        PbState.instance.states[PbState.instance.currentStateIndex] ={
            func: func,
            args: args
        }
        history.pushState({currentIndex: PbState.instance.currentStateIndex}, "")
        PbState.instance.currentStateIndex++;
    }
}