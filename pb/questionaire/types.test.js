import {expect, test} from '@jest/globals';
import {PbQuestion} from "./classes.js";


let qData = {
    "id": "q1ID",
    "text": "Text1",
    "title": 1,
    "next": "",
    "previous": ""
}

test('Create question', () => {
    let question = new PbQuestion(qData)
    expect(question.id).toStrictEqual(qData.id)
    expect(question.text).toStrictEqual(qData.text)
    expect(question.title).toStrictEqual(qData.title)
    expect(question.next).toStrictEqual(qData.next)
    expect(question.previous).toStrictEqual(qData.previous)
});

test("Test whether setting id of questions after constructing throws an error", () => {
    let question = new PbQuestion(qData)
    expect(() => {
        question.id = "foo"
    }).toThrow(TypeError);
})

test("Test whether changing question-values aside from id works", () => {
    let question = new PbQuestion(qData)
    question.text = "changed"
    question.text = "changed"
    question.title = "changed"
    question.next = "changed"
    question.previous = "changed"

    expect(question.text).toStrictEqual("changed")
    expect(question.title).toStrictEqual("changed")
    expect(question.next).toStrictEqual("changed")
    expect(question.previous).toStrictEqual("changed")
})

test('Create question', () => {
    let question = new PbQuestion(qData)
    expect(question.id).toStrictEqual(qData.id)
    expect(question.text).toStrictEqual(qData.text)
    expect(question.title).toStrictEqual(qData.title)
    expect(question.next).toStrictEqual(qData.next)
    expect(question.previous).toStrictEqual(qData.previous)
});

//# sourceMappingURL=types.test.js.map
