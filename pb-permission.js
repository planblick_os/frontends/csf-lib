import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {getCookie} from "./pb-functions.js?v=[cs_version]";

export class pbPermissions {
    roleset = {}
    rulesset = {}
    userrules = {}
    groups = {}
    my_groups = []
    userRuleSetTemplate = {
        "name": "",
        "description": "",
        "definition": {
            "import": [],
            "rules": {}
        }
    }

    constructor() {
        if (!pbPermissions.instance) {
            pbPermissions.instance = this;
        }

        return pbPermissions.instance;
    }


    init(role, force_reload = false) {
        return new Promise(function (resolve, reject) {

            var arr = [pbPermissions.instance.fetchGroupsFromApi(), pbPermissions.instance.fetchRolesFromApi(), pbPermissions.instance.fetchRulesFromApi(), pbPermissions.instance.fetchMyGroupsFromApi()]
            if (role != undefined) {
                arr.push(pbPermissions.instance.getRules(role, force_reload))
            }

            Promise.all(arr)
                .then(function (res) {
                    resolve()
                })
                .catch(function (err) {
                    reject()
                }) // This is executed
        })
    }

    setRole(rolename) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/addRightsRole",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(pbPermissions.instance.userrules[rolename]),
            };

            $.ajax(settings).done(function (response) {
                //console.log("RESPONSE", response)
                resolve()
            });
        })
    }

    setUserToGroup(rolename) {
        return new Promise(function (resolve, reject) {
            let data = pbPermissions.instance.userRuleSetTemplate
            let reinitModel = true
            if (pbPermissions.instance.roleset.roles[rolename]) {
                data = pbPermissions.instance.roleset.roles[rolename]
                reinitModel = false
            }
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/addRightsRole",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(data),
            };

            $.ajax(settings).done(function (response) {
                //console.log("RESPONSE", response)
                if (reinitModel) {
                    pbPermissions.instance.init(rolename, true)
                }
                resolve()
            });
        })
    }

    setGroup(groupname) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/addRightsRole",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(pbPermissions.instance.groups[groupname]),
            };

            $.ajax(settings).done(function (response) {
                //console.log("RESPONSE", response)
                resolve()
            });
        })
    }

    deleteGroup(groupname) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteRightsRole",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({"name": groupname}),
            };

            $.ajax(settings).done(function (response) {
                resolve()
            });
        })
    }

    getRules(role, force_reload = false) {
        if (!force_reload && pbPermissions.instance.rulesset[role] != undefined) {
            console.log("CACHED", role)
            return new Promise(function (resolve, reject) {
                resolve(pbPermissions.instance.rulesset[role])
            })


        } else {
            console.log("NOT CACHED", role)
            return new Promise(function (resolve, reject) {
                pbPermissions.instance.fetchRulesFromApi(role).then(function () {
                    resolve(pbPermissions.instance.rulesset[role])
                })
            })
        }
    }

    fetchRulesFromApi(role) {
        return new Promise(function (resolve, reject) {
            let called_url = "/get_rights"
            let role_name = role
            if (role != undefined && role != false && !role.includes("false") && !role.includes("undefined")) {
                called_url += "/" + role
            } else {
                role = "default"
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + called_url,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
            };

            $.ajax(settings).done(function (response) {

                pbPermissions.instance.rulesset[role] = response
                resolve()
            });
        })
    }

    fetchRolesFromApi() {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_roles",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
            };

            $.ajax(settings).done(function (response) {
                pbPermissions.instance.roleset = response
                resolve(pbPermissions.instance.roleset)
            });
        })
    }

    fetchGroupsFromApi(withMembers = false) {
        return new Promise(function (resolve, reject) {
            let url = "/get_groups"
            if(withMembers) {
                url += "?include_members=true"
            }
            var settings = {
                "url": CONFIG.API_BASE_URL + url,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
            };

            $.ajax(settings).done(function (response) {
                pbPermissions.instance.groups = response.groups
                resolve(pbPermissions.instance.groups)
            });
        })
    }

    fetchMyGroupsFromApi() {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_my_groups",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": function (response) {
                    pbPermissions.instance.my_groups = []
                    response.groups.forEach(group => {
                        if (group.includes("group.")) {
                            pbPermissions.instance.my_groups.push(group)
                        }
                    });

                    resolve(pbPermissions.instance.my_groups)
                },
                "error": function (xhr, ajaxOptions, thrownError) {
                    reject(xhr, ajaxOptions, thrownError)
                }
            };

            $.ajax(settings)
        })
    }

    fetchGroupMembers(group_name) {
        group_name = group_name.replace("group.", "")
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_group_members/" + group_name,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": function (response) {
                    resolve(response.members)
                },
                "error": function (xhr, ajaxOptions, thrownError) {
                    reject(xhr, ajaxOptions, thrownError)
                }
            };

            $.ajax(settings)
        })
    }

    getAbsoluteRightsOfRole(role) {
        return new Promise(function (resolve, reject) {
            if (!getCookie("apikey")) {
                resolve()
                return
            }

            if (role == undefined || role == false || role.includes("false") || role.includes("undefined")) {
                role = ""
            } else {
                role = "/" + role
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_rights" + role,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": function (response) {
                    resolve(response)
                },
                "error": function () {
                    resolve()
                }
            }

            $.ajax(settings)
        })
    }

    deleteRole(rolename) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteRightsRole",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({"name": rolename}),
            };

            $.ajax(settings).done(function (response) {
                //console.log("RESPONSE", response)
                resolve()
            });
        })
    }

    getRightsRoles() {
        return pbPermissions.instance.roleset
    }

    getRoleSetByName(name) {
        if (pbPermissions.instance.roleset.roles[name]) {
            return pbPermissions.instance.roleset.roles[name]
        } else {
            return undefined
        }
    }
}
