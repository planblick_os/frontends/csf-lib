import {fireEvent, waitFor} from "./pb-functions.js?v=[cs_version]"

export class pbMedia {
    videoElement = null
    audioInputSelect = null
    audioOutputSelect = null
    videoSelect = null
    selectors = []
    stream = null
    currentVideoDevice = null
    currentAudioDevice = null
    previousVideoSource = null
    previousAudioSource = null
    allDevices = {}
    alreadyRunning = false

    constructor(containerElement = null, mode = "videoWithAudioInput") {
        if (!pbMedia.instance) {
            if (mode == "videoOnly") {
                this.videoElement = this.getVideoElement()
                this.attachElementToContainer(containerElement, this.videoElement)
                this.videoSelect = this.getVideoSelect()
                this.attachElementToContainer(containerElement, this.videoSelect)
                this.selectors = [this.videoSelect]
            }

            if (mode == "videoWithAudioInput") {
                this.videoElement = this.getVideoElement()
                this.attachElementToContainer(containerElement, this.videoElement)
                this.videoSelect = this.getVideoSelect()
                this.attachElementToContainer(containerElement, this.videoSelect)
                this.audioInputSelect = this.getAudioInputSelect()
                this.attachElementToContainer(containerElement, this.audioInputSelect)

                this.selectors = [this.audioInputSelect, this.videoSelect]
            }

            if (mode == "videoWithAudioInputAndOutput") {
                this.videoElement = this.getVideoElement()
                this.attachElementToContainer(containerElement, this.videoElement)
                this.videoSelect = this.getVideoSelect()
                this.attachElementToContainer(containerElement, this.videoSelect)
                this.audioInputSelect = this.getAudioInputSelect()
                this.attachElementToContainer(containerElement, this.audioInputSelect)
                this.audioOutputSelect = this.getAudioOutputSelect()
                this.attachElementToContainer(containerElement, this.audioOutputSelect)

                if (this.audioOutputSelect) {
                    this.audioOutputSelect.disabled = !('sinkId' in HTMLMediaElement.prototype);
                }

                this.selectors = [this.audioInputSelect, this.audioOutputSelect, this.videoSelect]
            }

            if (this.audioInputSelect)
                this.audioInputSelect.onchange = this.start;
            if (this.audioOutputSelect)
                this.audioOutputSelect.onchange = this.changeAudioDestination;
            if (this.videoSelect)
                this.videoSelect.onchange = this.start;

            this.videoElement.volume = 0
            pbMedia.instance = this
        }

        return pbMedia.instance;
    }

    init() {

        navigator.mediaDevices.enumerateDevices().then(pbMedia.instance.gotDevices)//.catch(pbMedia.instance.handleError);
        return pbMedia.instance
    }

    getStream() {
        if(!pbMedia.instance || !pbMedia.instance.stream) {

        } else {
            return pbMedia.instance.stream
        }
    }

    handleError(error) {
        console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
    }

    attachElementToContainer(container, element) {
        if (container) {
            container.appendChild(element)
        }
    }

    getVideoElement() {
        let videoElement = document.querySelector('video#pbMediaVideo')
        if (!videoElement) {
            videoElement = document.createElement("video");
            videoElement.id = "pbMediaVideo"
            videoElement.autoplay = true;
            videoElement.playsinline = true;
            videoElement.style.width = "640px"
            videoElement.style.height = "480px"

        }

        return videoElement
    }

    getVideoSelect() {
        let videoSelect = document.querySelector('select#videoSourceSelect')
        if (!videoSelect) {
            videoSelect = document.createElement("select");
            videoSelect.id = "videoSourceSelect"
        }
        let option = document.createElement('option');
        option.value = "0"
        option.text = "Video-Quelle ändern"
        videoSelect.appendChild(option)
        return videoSelect
    }

    getAudioInputSelect() {
        let audioInputSelect = document.querySelector('select#audioSourceSelect')
        if (!audioInputSelect) {
            audioInputSelect = document.createElement("select");
            audioInputSelect.id = "audioSourceSelect"
        }
        let option = document.createElement('option');
        option.value = "0"
        option.text = "Audio-Quelle ändern"
        audioInputSelect.appendChild(option)
        return audioInputSelect
    }

    getAudioOutputSelect() {
        let audioOutputSelect = document.querySelector('select#audioOutputSelect')
        if (!audioOutputSelect) {
            audioOutputSelect = document.createElement("select");
            audioOutputSelect.id = "audioOutputSelect"
        }
        let option = document.createElement('option');
        option.value = "0"
        option.text = "Audio-Ausgabe ändern"
        audioOutputSelect.appendChild(option)
        return audioOutputSelect
    }

    gotDevices(deviceInfos) {
        // Handles being called several times to update labels. Preserve values.
        const values = pbMedia.instance.selectors.map(select => select.value);
        pbMedia.instance.selectors.forEach(select => {
            while (select.lastChild) {
                if (select.lastChild.value == 0)
                    break;
                select.removeChild(select.lastChild);
            }
        });
        for (let i = 0; i !== deviceInfos.length; ++i) {
            const deviceInfo = deviceInfos[i];

            pbMedia.instance.allDevices[deviceInfo.deviceId] = deviceInfo

            const option = document.createElement('option');
            option.value = deviceInfo.deviceId;
            if (pbMedia.instance.audioInputSelect && deviceInfo.kind === 'audioinput') {
                option.text = deviceInfo.label || `microphone ${pbMedia.instance.audioInputSelect.length + 1}`;
                pbMedia.instance.audioInputSelect.appendChild(option);
            } else if (pbMedia.instance.audioOutputSelect && deviceInfo.kind === 'audiooutput') {
                option.text = deviceInfo.label || `speaker ${pbMedia.instance.audioOutputSelect.length + 1}`;
                pbMedia.instance.audioOutputSelect.appendChild(option);
            } else if (pbMedia.instance.videoSelect && deviceInfo.kind === 'videoinput') {
                option.text = deviceInfo.label || `camera ${pbMedia.instance.videoSelect.length + 1}`;
                pbMedia.instance.videoSelect.appendChild(option);
            } else {
                //console.log('Some other kind of source/device: ', deviceInfo);
            }
        }
        console.log("DEVICE INFOS", pbMedia.instance.allDevices)
        pbMedia.instance.selectors.forEach((select, selectorIndex) => {
            if (Array.prototype.slice.call(select.childNodes).some(n => n.value === values[selectorIndex])) {
                select.value = values[selectorIndex];
            }
        });
        fireEvent("gotMultimediaDevices")

    }

    attachSinkId(element, sinkId) {
        if (typeof element.sinkId !== 'undefined') {
            element.setSinkId(sinkId)
                .then(() => {
                    console.log(`Success, audio output device attached: ${sinkId}`);
                })
                .catch(error => {
                    let errorMessage = error;
                    if (error.name === 'SecurityError') {
                        errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
                    }
                    console.error(errorMessage);
                    // Jump back to first output device in the list as it's the default.
                    pbMedia.instance.audioOutputSelect.selectedIndex = 0;
                });
        } else {
            console.warn('Browser does not support output device selection.');
        }
    }

    changeAudioDestination() {
        const audioDestination = pbMedia.instance.audioOutputSelect.value;
        pbMedia.instance.attachSinkId(pbMedia.instance.videoElement, audioDestination);
    }

    gotStream(stream, onStreamStarted = null) {
        window.stream = stream; // make stream available to console
        pbMedia.instance.stream = stream
        pbMedia.instance.videoElement.srcObject = stream;

        stream.getTracks().forEach(track => {
            pbMedia.instance.setLastUsedDeviceIds(track.getSettings().deviceId)
        });

        console.log("onStreamStarted", onStreamStarted)

        if (onStreamStarted && typeof onStreamStarted == "function") {
            onStreamStarted();
        }

        fireEvent("streamRunning")
        // Refresh button list in case labels have become available
        return navigator.mediaDevices.enumerateDevices();
    }

    start(onStreamStarted = null) {
        let restartStream = true
        pbMedia.instance.alreadyRunning = true

        if (window.stream && pbMedia.instance.videoSelect && pbMedia.instance.videoSelect.value != "0") {
            window.stream.getTracks().forEach(track => {
                track.stop();
            });
        } else if(pbMedia.instance.videoSelect && pbMedia.instance.videoSelect.value != "0") {
            return
        }

        var audioSource = undefined
        var videoSource = undefined


        if (pbMedia.instance.audioInputSelect && pbMedia.instance.audioInputSelect.value != "0") {
            audioSource = pbMedia.instance.audioInputSelect.value;
        } else {
            audioSource = pbMedia.instance.getLastSelectedAudioSource()
        }

        if (pbMedia.instance.videoSelect && pbMedia.instance.videoSelect.value != "0") {
            videoSource = pbMedia.instance.videoSelect.value;
        } else {
            videoSource = pbMedia.instance.getLastSelectedVideoSource()
        }

        var constraints = {
            audio: {deviceId: audioSource ? {exact: audioSource} : true},
            video: {deviceId: videoSource ? {exact: videoSource} : undefined}
        }

        if (audioSource && videoSource) {
            constraints = {
                audio: {deviceId: audioSource ? {exact: audioSource} : undefined},
                video: {deviceId: videoSource ? {exact: videoSource} : undefined}
            }
        }

        if (pbMedia.instance.previousVideoSource !== videoSource || pbMedia.instance.previousAudioSource !== audioSource) {
            let result = navigator.mediaDevices.getUserMedia(constraints).then((stream) => pbMedia.instance.gotStream(stream, onStreamStarted)).then(pbMedia.instance.gotDevices)//.catch(pbMedia.instance.handleError);
            pbMedia.instance.previousVideoSource = videoSource;
            pbMedia.instance.previousAudioSource = audioSource;
            return result
        } else {
            if (onStreamStarted && typeof onStreamStarted == "function") {
                onStreamStarted();
            }
        }
    }

    getLastSelectedAudioSource() {
        var match = document.cookie.match(new RegExp('(^| )' + "LastSelectedAudioSource" + '=([^;]+)'));
        if (match) return match[2];
    }

    getLastSelectedVideoSource() {
        var match = document.cookie.match(new RegExp('(^| )' + "LastSelectedVideoSource" + '=([^;]+)'));
        if (match) return match[2];
    }

    setLastUsedAudioSource(source) {
        document.cookie = "LastSelectedAudioSource=" + source + "; expires=Fri, 01 Jan 2500 12:00:00 UTC; SameSite=Lax";
    }

    setLastUsedVideoSource(source) {
        document.cookie = "LastSelectedVideoSource=" + source + "; expires=Fri, 01 Jan 2500 12:00:00 UTC; SameSite=Lax";
    }

    setLastUsedDeviceIds(deviceId) {
        navigator.mediaDevices.enumerateDevices()
            .then(function (devices) {
                devices.forEach(function (device) {
                    if (deviceId == device.deviceId) {
                        if (device.kind == "videoinput") {
                            pbMedia.instance.currentVideoDevice = device;
                            pbMedia.instance.setLastUsedVideoSource(deviceId)
                            console.log("VIDEO DEVICE", device)
                        }
                        if (device.kind == "audioinput") {
                            pbMedia.instance.currentAudioDevice = device;
                            pbMedia.instance.setLastUsedAudioSource(deviceId);
                            console.log("AUDIO DEVICE", device)
                        }
                    }
                });
            })
            .catch(function (err) {
                console.log(err.name + ": " + err.message);
            });
    }

    getDeviceById(deviceId) {
        if (pbMedia.instance.allDevices[deviceId])
            return pbMedia.instance.allDevices[deviceId]
        else
            return null
    }

    getCurrentVideoDevice() {
        // not properly working so far due to raceconditions
        return pbMedia.instance.getDeviceById(pbMedia.instance.getLastSelectedVideoSource())
    }

    getCurrentAudioDevice() {
        // not properly working so far due to raceconditions
        return pbMedia.instance.getDeviceById(pbMedia.instance.getLastSelectedAudioSource())
    }
}