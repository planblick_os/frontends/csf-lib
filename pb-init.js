import {CONFIG} from '../pb-config.js?v=[cs_version]';
import {fireEvent, getCookie, waitFor} from "./pb-functions.js?v=[cs_version]";
import {socketio} from "./pb-socketio.js?v=[cs_version]";
import {pbI18n} from "./pb-i18n.js?v=[cs_version]"

export class pbInit {
    constructor(needsToBeLoggedIn=true) {
        this.systems_ready = {
            "js_sources_loaded": false,
        }

        if(getCookie("apikey")) {
            this.systems_ready["socketio_connected"] = false
        }

        this.loading_files = {}
        this.needed_js_files = CONFIG.NEEDED_JS_FILES
        if (!pbInit.instance) {
            pbInit.instance = this;
        }
        pbInit.instance.system_ready_check_starttime = Math.floor(Date.now() / 1000)

        return pbInit.instance;
    }

    now() {
        document.addEventListener("js_sources_loaded", function (event) {
            new pbI18n().init()
            if(true || getCookie("apikey")) {
                new socketio().init()
            }
        })

        document.addEventListener("socketio_cannotconnect", function (event, needsToBeLoggedIn) {
            if(!needsToBeLoggedIn) {
                pbInit.instance.systems_ready["socketio_connected"] = true
            }
            console.warn("Cannot connect to socketio")
        })

        document.addEventListener("socketio_connected", function (event) {
            pbInit.instance.systems_ready["socketio_connected"] = true
            document.addEventListener("streamForTranscription", function (event) {
                socketio.instance.socket.emit('streamForTranscription', event.data)
            })
        })

        this.load_needed_js_files()


        waitFor(this.is_system_ready, pbInit.instance.system_ready)
    }

    system_ready() {
        fireEvent("system-initialized")
        const url = new URL(window.location.href);
        url.searchParams.delete('reloaded');
        window.history.replaceState(null, null, url);
        Fnon.Wait.Remove()
    }

    load_needed_js_files() {
        window.jQuery.ajaxSetup({
            cache: CONFIG.JS_CACHE_ACTIVE
        });
        for (let file of this.needed_js_files) {
            this.load_file(file)
        }

        waitFor(this.finished_loading_files, pbInit.instance.files_loaded)

    }

    load_file(file) {
        pbInit.instance.loading_files[file.name] = true
        window.jQuery.getScript(file.path)
            .done(function (script, textStatus) {
                fireEvent(file.name + '_loaded')
                pbInit.instance.loading_files[file.name] = false
            })
            .fail(function (jqxhr, settings, exception) {
                file.loaded = false
                console.warn(file.path, exception);
                fireEvent('js_file_load_failed', file)
            });
    }

    finished_loading_files() {
        let still_loading = false
        for (let key of Object.keys(pbInit.instance.loading_files)) {
            if (pbInit.instance.loading_files[key] == true) {
                still_loading = true
            }
        }
        console.info("Files still loading: ", still_loading, pbInit.instance.loading_files)
        return !still_loading
    }

    is_system_ready() {
        let still_loading = false
        for (let key of Object.keys(pbInit.instance.systems_ready)) {
            if (pbInit.instance.systems_ready[key] == false) {
                still_loading = true
            }
        }
        let loading_since = Math.floor(Date.now() / 1000) - pbInit.instance.system_ready_check_starttime
        console.info("System ready: ", still_loading, loading_since, pbInit.instance.systems_ready)

        Fnon.Wait.Init({
            clickToClose: false,
            svgColor: "#00a99d",
            textColor: "#00a99d",
            background: "rgba(255,255,255,1)",
            containerSize: "1024px"
        });

        if(loading_since == 1) {
            Fnon.Wait.Typing(new pbI18n().translate("Loading"))
        }
        if(loading_since > 5) {
            $("#loader-overlay").hide()
            Fnon.Wait.Change(new pbI18n().translate("Sorry for the delay, we're still trying to establish a live-connection..."));
        }

        if(loading_since > 15) {
            Fnon.Wait.Change(new pbI18n().translate("This takes way longer than usual. Maybe you lost your Internet-Connection?"));
        }

        if(loading_since > 25) {
            Fnon.Wait.Change(new pbI18n().translate("Giving up in 5 Seconds and performing automatic reload..."));
        }

        if(loading_since > 30) {
            const url = new URL(window.location.href);
            url.searchParams.set('reloaded', Math.floor(Date.now() / 1000))
            window.location.href = url
        }

        return !still_loading
    }

    files_loaded() {
        pbInit.instance.systems_ready["js_sources_loaded"] = true
        fireEvent("js_sources_loaded")
    }
}