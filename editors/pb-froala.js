import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {getCookie} from "../pb-functions.js?v=[cs_version]"
import {PbAccount} from "../pb-account.js?v=[cs_version]"


// Tested options of froala
// this.options = {
//         key: CONFIG.FROALA_KEY,
//         attribution: false,
//         colorsText: [
//             '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
//             '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
//         ],
//         colorsBackground: [
//             '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
//             '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
//         ],
//         colorsStep: 6,
//         imageInsertButtons: ['imageBack', '|', 'imageByURL'],
//         videoInsertButtons: ['videoBack', '|', 'videoByURL', 'videoEmbed', 'videoUpload],
//         toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'backgroundColor', 'textColor', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'trackChanges', 'markdown']
//     }
// }

export class PbFroala {
    constructor(targetSelector, onUploadStartFunc = () => "", onUploadFinishedFunc = () => "", options = undefined) {
        let uploads = []
        this.onUploadStartFunc = onUploadStartFunc
        this.onUploadFinishedFunc = onUploadFinishedFunc
        let self = this
        if (options) {
            this.options = options
        } else {
            if (PbAccount.instance?.accountProfileData?.filestorage_webdav?.hostname?.length > 0) {
                this.options = {
                    key: CONFIG.FROALA_KEY,
                    pluginsEnabled: ['align', 'charCounter', 'codeBeautifier', 'codeView', 'colors', 'draggable', 'embedly', 'emoticons', 'entities', 'file', 'fontAwesome', 'fontFamily', 'fontSize', 'fullscreen', 'image', 'imageTUI', 'inlineStyle', 'inlineClass', 'lineBreaker', 'lineHeight', 'link', 'lists', 'paragraphFormat', 'paragraphStyle', 'quickInsert', 'quote', 'save', 'table', 'url', 'video', 'wordPaste'],
                    fileUploadURL: CONFIG.API_BASE_URL + '/froala/upload_file?apikey=' + getCookie("apikey"),
                    fileMaxSize: 1024 * 1024 * 300, // Last number represents desired MB
                    imageUploadURL: CONFIG.API_BASE_URL + '/froala/upload_image?apikey=' + getCookie("apikey"),
                    imageUploadRemoteUrls: false,
                    imageMaxSize: 1024 * 1024 * 10, // Last number represents desired MB
                    videoUploadURL: CONFIG.API_BASE_URL + '/froala/upload_video?apikey=' + getCookie("apikey"),
                    videoMaxSize: 1024 * 1024 * 300, // Last number represents desired MB
                    heightMin: 300,
                    heightMax: 600,
                    placeholderText: new pbI18n().translate('Insert your text here'),
                    language: 'de',
                    attribution: false,

                    colorsText: [
                        '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
                        '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
                    ],
                    colorsBackground: [
                        '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
                        '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
                    ],
                    colorsStep: 6,
                    imagePaste: true,
                    imagePasteProcess: true,
                    imageAllowedTypes: ['jpeg', 'jpg', 'png'],
                    pasteDeniedTags: [],
                    pastePlain: false,
                    events: {
                        'file.beforeUpload': function (files) {
                            return self.onUploadStart("file", files)
                        },
                        'image.beforeUpload': function (images) {
                            return self.onUploadStart("image", images)
                        },
                        'video.beforeUpload': function (videos) {
                            return self.onUploadStart("video", videos)
                        },
                        'edit.off': function (images) {
                            //self.onUploadStart("image", images)
                        },
                        'image.uploaded': function (response) {
                            self.onUploadFinished(response)
                            //self.onUploadFinished(response)
                        },
                        'video.uploaded': function (response) {
                            // Do something here.
                            // this is the editor instance.
                            self.onUploadFinished(response)
                        },
                        'file.uploaded': function (response) {
                            self.onUploadFinished(response)
                        },
                        'image.removed': function (image) {
                            console.log("IMAGE REMOVED", image, image[0].src)
                            self.deleteRemoteFile(image[0].src)
                        },
                        'video.removed': function (video) {
                            console.log("VIDEO REMOVED", video, video[0].src)
                            self.deleteRemoteFile(video[0].src)
                        },
                        'file.unlink': function (file) {
                            console.log("FILE REMOVED", file, file.href)
                            self.deleteRemoteFile(file.href)
                        },
                        'edit.on': function (img) {
                            //self.onUploadFinished(img)
                        },
                        'image.beforePasteUpload': function (images) {
                            //return false
                        },
                        'paste.before': function (original_event) {
                            if (original_event.clipboardData.getData('Text').length == 0) {
                                return false
                            }
                        },
                        'contentChanged': function () {

                        },
                        'commands.before': function (cmd, param1, param2) {
                            console.log("CMD", cmd, param1, param2)
                        }
                    },
                    toolbarButtons: {
                        'moreText': {
                            'buttons': [
                                'bold',
                                'italic',
                                'underline',
                                'strikeThrough',
                                'subscript',
                                'superscript',
                                'fontFamily',
                                'fontSize',
                                'textColor',
                                'backgroundColor',
                                'inlineClass',
                                'inlineStyle',
                                'clearFormatting'
                            ],
                            'align': 'left',
                            'buttonsVisible': 3
                        },
                        'moreParagraph': {
                            'buttons': [
                                'alignLeft',
                                'alignCenter',
                                'formatOLSimple',
                                'alignRight',
                                'alignJustify',
                                'formatOL',
                                'formatUL',
                                'paragraphFormat',
                                'paragraphStyle',
                                'lineHeight',
                                'outdent',
                                'indent',
                                'quote'
                            ],
                            'align': 'left',
                            'buttonsVisible': 2
                        },
                        'moreRich': {
                            'buttons': [
                                'insertLink',
                                'insertImage',
                                'insertVideo',
                                'insertFile',
                                'insertTable',
                                'emoticons',
                                'fontAwesome',
                                'specialCharacters',
                                'embedly',
                                'insertHR'
                            ],
                            'align': 'left',
                            'buttonsVisible': 2
                        },
                        'moreMisc': {
                            'buttons': [
                                'undo',
                                'redo',
                                'fullscreen',
                                'print',
                                'spellChecker',
                                'selectAll',
                                'html',
                                'help'
                            ],
                            'align': 'right',
                            'buttonsVisible': 2
                        }
                    }
                }
            } else {
                this.options = {
                    key: CONFIG.FROALA_KEY,
                    // fileUploadURL: CONFIG.API_BASE_URL + '/froala/upload_file?apikey=' + getCookie("apikey"),
                    // imageUploadURL: CONFIG.API_BASE_URL + '/froala/upload_image?apikey=' + getCookie("apikey"),
                    // videoUploadURL: CONFIG.API_BASE_URL + '/froala/upload_video?apikey=' + getCookie("apikey"),
                    imageUploadRemoteUrls: false,
                    heightMin: 300,
                    heightMax: 600,
                    placeholderText: new pbI18n().translate('Insert your text here'),
                    language: 'de',
                    attribution: false,
                    colorsText: [
                        '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
                        '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
                    ],
                    colorsBackground: [
                        '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
                        '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
                    ],
                    colorsStep: 6,
                    imagePaste: false,
                    imagePasteProcess: false,
                    imageAllowedTypes: [],
                    pasteDeniedTags: ['img'],
                    pastePlain: false,
                    imageInsertButtons: ['imageBack', '|', 'imageByURL'],
                    videoInsertButtons: ['videoBack', '|', 'videoByURL', 'videoEmbed'],
                    events: {
                        'image.beforePasteUpload': function (images) {
                            return false
                        },
                        'paste.before': function (original_event) {
                            if (original_event.clipboardData.getData('Text').length == 0) {
                                return false
                            }
                        }
                    },
                    toolbarButtons: {
                        'moreText': {
                            'buttons': [
                                'bold',
                                'italic',
                                'underline',
                                'strikeThrough',
                                'subscript',
                                'superscript',
                                'fontFamily',
                                'fontSize',
                                'textColor',
                                'backgroundColor',
                                'inlineClass',
                                'inlineStyle',
                                'clearFormatting'
                            ]
                        },
                        'moreParagraph': {
                            'buttons': [
                                'alignLeft',
                                'alignCenter',
                                'formatOLSimple',
                                'alignRight',
                                'alignJustify',
                                'formatOL',
                                'formatUL',
                                'paragraphFormat',
                                'paragraphStyle',
                                'lineHeight',
                                'outdent',
                                'indent',
                                'quote'
                            ]
                        },
                        'moreRich': {
                            'buttons': [
                                'insertLink',
                                //'insertImage',
                                'insertVideo',
                                //'insertFile',
                                'insertTable',
                                //'emoticons',
                                'fontAwesome',
                                'specialCharacters',
                                'embedly',
                                'insertHR'
                            ],
                            'buttonsVisible': 4
                        },
                        'moreMisc': {
                            'buttons': [
                                'undo',
                                'redo',
                                'fullscreen',
                                'print',
                                'spellChecker',
                                'selectAll',
                                'html',
                                'help'
                            ],
                            'align': 'right',
                            'buttonsVisible': 2
                        }
                    }
                }
            }
        }
        let targetElement = document.querySelector(targetSelector);
        if (!targetElement) {
            console.warn("Target-selector for froala not found:", targetSelector)
        } else {
            this.targetSelector = targetSelector
        }


        // this.options = {}
        // if (options)
        //     this.options = Object.assign(this.options, options)

        return this
    }

    init() {
        $('<link/>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: '/csf-lib/editors/froala/css/froala_editor.pkgd.min.css?v=[cs_version]'
        }).appendTo('head');

        return new Promise((resolve, reject) => {
            var url = "/csf-lib/editors/froala/js/froala_editor.pkgd.min.js?v=[cs_version]"
            let self = this
            $.ajaxSetup({cache: false});
            if (typeof FroalaEditor == "undefined") {
                $.getScript(url, function () {
                    let language_file_url = "/csf-lib/editors/froala/js/languages/de.js?v=[cs_version]"
                    $.getScript(language_file_url, function () {
                        self.raw_editor = new FroalaEditor(self.targetSelector, self.options, () => {
                            resolve(self)
                        })
                    })
                })
            } else {
                self.raw_editor = new FroalaEditor(self.targetSelector, self.options, () => {
                    resolve(self)
                });
            }
        })
    }

    setContent(content) {
        this.raw_editor.html.set(content)
    }

    onUploadStart(type, params) {
        if(params[0].name.length > 180) {
            alert(new pbI18n().translate("The filename is too long. Please rename the file and try again."))
            return false
        }
        return this.onUploadStartFunc(type, params)
    }

    async deleteRemoteFile(url) {
        new Promise((resolve, reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/froala/delete",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "file_path": decodeURI(url)
                }),
                "success": (response) => {
                    const customEvent = new CustomEvent('froala.remoteFileDeleted', {
                        detail: {url: decodeURI(url)}
                    });
                    document.dispatchEvent(customEvent);
                    resolve(response)
                },
                "error": () => {
                    resolve()
                }
            };
            $.ajax(settings)
        })
    }

    onUploadFinished(response) {
        this.onUploadFinishedFunc(response)
    }

    getContent() {
        return this.raw_editor.html.get()
    }

    onTextChanged(func) {
        this.raw_editor.events.on("contentChanged", func)
    }

    testWebDav(onSuccess, onError) {
        return new Promise((resolve = () => "", reject = () => "") => {
            let settings = {
                "url": CONFIG.API_BASE_URL + "/froala/webdav/free",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "success": () => {
                    onSuccess()
                    resolve()
                },
                "error": () => {
                    onError()
                    reject()
                }
            };

            $.ajax(settings)
        })
    }
}
