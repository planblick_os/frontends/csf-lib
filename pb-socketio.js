import {CONFIG} from '../pb-config.js?v=[cs_version]'
import {getCookie, fireEvent, md5, loadNotifications} from './pb-functions.js?v=[cs_version]'
import {PbAccount} from "./pb-account.js?v=[cs_version]"
import {PbNotifications} from "./pb-notifications.js?v=[cs_version]"

export class socketio {
    constructor() {
        this.handled_messages = []
        if (socketio.instance) {
            return socketio.instance;
        }
        socketio.instance = this;
        socketio.instance.wasConnected = false
        socketio.instance.reloadHintIsShown = false
        socketio.instance.namespace = "/"

        socketio.instance.commandhandlers = {
            fireEvent: function (args) {
                fireEvent(args.eventname, args.eventdata)
            },
            fireBackendEvent: function (args) {
                fireEvent(args.eventname, args.eventdata)
            },
            showAlert: function (args) {
                alert(args.text)
            },
            browserNotification: function (message) {
                console.log("Received browser notification:", message)
                if(message.types.includes("showHint")) {
                    new PbNotifications().showHint(message?.params?.message)
                }
                if(message.types.includes("showAlert")) {
                    new PbNotifications().showAlert(message?.params?.message, message?.params?.title)
                }
                loadNotifications()
            },
            showNotification: function (args) {
                let hash = md5(args.task_id + args.text)
                if(!socketio.instance.handled_messages.includes(hash)) {
                    socketio.instance.handled_messages.push(hash)
                    new PbNotifications().showHint(args.text)
                }
            },
            forcereload: function (args) {
                console.log(args)
                let reason = args.reason
                alert("Die Seite muss wegen " + reason + " aktualisiert werden.\nNach bestätigen dieser Meldung erledigen wir das automatisch für Sie.")
                location.reload();
            },
            reloadPage: function (args) {
                location.reload();
            },
            refreshAccessRights: function (args) {
                new PbAccount().refreshPermissions()
            }
        }
        socketio.instance.eventhandlers = {}

        return socketio.instance
    }

    init() {
        //
        console.log("Listening for messages on:" + CONFIG.SOCKET_SERVER + socketio.instance.namespace)
        if(!getCookie("apikey")) {
            fireEvent("socketio_cannotconnect")
            return self
        }
        socketio.instance.socket = io(CONFIG.SOCKET_SERVER + socketio.instance.namespace, {
            path: CONFIG.SOCKETIO_SUBPATH + '/socket.io',
            query: {apikey: getCookie("apikey")}
        });
        var onevent = socketio.instance.socket.onevent;
        socketio.instance.socket.onevent = function (packet) {
            var args = packet.data || [];
            onevent.call(this, packet);    // original call
            packet.data = ["*"].concat(args);
            onevent.call(this, packet);      // additional call to catch-all
        };

        let connectionDropHandler = function() {
            if(socketio.instance.wasConnected && socketio.instance.reloadHintIsShown != true) {
                socketio.instance.reloadHintIsShown = true
                new PbNotifications().ask("Lost live-connection. You should reload this page to prevent data-loss. Shall we reload the page now?",
                                          "Lost live-connection",
                                          ()=>location.reload(true),
                                          ()=>{setTimeout(()=>{socketio.instance.reloadHintIsShown = false; connectionDropHandler()}, 60000)},
                                          "warning",
                                          "Reload",
                                          "Wait 60s")
            }
        }

        socketio.instance.socket.on('connect_error', function () {
            fireEvent("socket_connection_lost")
            connectionDropHandler()
        })

        socketio.instance.socket.on('connect', function () {
            console.log("Connected to socketio server")
            socketio.instance.wasConnected = true
            fireEvent("socketio_connected")
            socketio.instance.socket.emit('join', {room: "tmp_global"});
            if (getCookie("consumer_name"))
                socketio.instance.socket.emit('join', {room: getCookie("consumer_name")});
            if (getCookie("consumer_id"))
                socketio.instance.socket.emit('join', {room: getCookie("consumer_id")});
            if (getCookie("username"))
                socketio.instance.socket.emit('join', {room: decodeURIComponent(getCookie("username"))});
        });

        socketio.instance.socket.on('reconnect', function () {
          //alert("Wir haben leider die Verbindung zum Server verloren. Die Seite wird jetzt neu geladen um die Verbindung wiederherzustellen.")
          //location.reload()
        })

        socketio.instance.socket.on("playresponse", (input) => {
            socketio.instance.playByteArray(input)
        });

        socketio.instance.socket.on("*", function (event, data) {
            console.info("Received message from socketio:", event, data)
            fireEvent(event, data)
            if (typeof (data) == "string") {
                data = JSON.parse(data)
            }

            var payload = null;
            if (typeof (data.data) == "undefined" && data["data"]) {
                payload = JSON.parse(data["data"])
            } else {
                payload = data.data;
            }
            if (!payload) {
                payload = data
            }

            if (event == "command" && socketio.instance.commandhandlers[payload.command]) {
                socketio.instance.commandhandlers[payload.command](payload.args)
            }
            if (event == "event" && socketio.instance.eventhandlers[payload.event]) {
                socketio.instance.eventhandlers[payload.event](payload.args)
            }
        });

        return self
    }

    joinRoom(room) {
        socketio.instance.socket.emit('join', {room: decodeURIComponent(room)});
    }

    playByteArray(arrayBuffer) {
        if (!socketio.instance.context) {
            socketio.instance.context = new AudioContext();
        }
        socketio.instance.context.decodeAudioData(arrayBuffer, function (buffer) {
            socketio.instance.play(buffer);
        });
    }

    play(buffer) {
        // Create a source node from the buffer
        var source = socketio.instance.context.createBufferSource();
        source.buffer = buffer;
        // Connect to the final output node (the speakers)
        source.connect(socketio.instance.context.destination);
        // Play immediately
        source.start(0);
        source.onended = function () {

        }
    }

}
