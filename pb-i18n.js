import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {fireEvent, getCookie} from "./pb-functions.js?v=[cs_version]"

export class pbI18n {
    constructor() {
        if (!pbI18n.instance) {
            pbI18n.instance = this;
            pbI18n.instance.i18n = {}
            document.addEventListener("i18nLanguageLoaded", function _(event) {
                pbI18n.instance.replaceI18n()
            })
        }
        return pbI18n.instance;
    }

    init() {

        pbI18n.instance.getTranslations()

        return pbI18n.instance
    }

    translate = function (vocab) {
        if (pbI18n.instance.i18n[vocab]) {
            return pbI18n.instance.i18n[vocab]
        } else {
            //console.warn("Not found translation for vocab: ", vocab)
            return vocab
        }
    }

    getAvailableBrowserLanguage = function (available_languages = CONFIG.AVAILABLE_LANGUAGES) {
        let lang = navigator.languages ? navigator.languages[0] : navigator.language;
        let ISOA2 = lang.substr(0, 2);

        if (available_languages.includes(ISOA2)) {
            return ISOA2
        } else {
            return "de"
        }
    }

    getTranslations = function (language = null) {
        if (language == null || language == undefined) {
            language = pbI18n.instance.getAvailableBrowserLanguage()
        }

        let coreurl = "/core_" + language + ".json?v=[cs_version]"
        let promises = []
        pbI18n.instance.loadLanguageFile(coreurl, pbI18n.instance.setI18n, true).then(function () {
            let url = "i18n/" + language + ".json?v=[cs_version]"
            pbI18n.instance.loadLanguageFile(url, pbI18n.instance.setI18n, true).then(function () {
                let prefix_language = undefined
                if (CONFIG.LANGUAGEFILE_PREFIX) {
                    prefix_language = `i18n/${CONFIG.LANGUAGEFILE_PREFIX}${language}.json?v=[cs_version]`
                    promises.push(pbI18n.instance.loadLanguageFile(prefix_language, pbI18n.instance.setI18n, true))
                }

                url = "./i18n/" + language + ".json?v=[cs_version]"
                promises.push(pbI18n.instance.loadLanguageFile(url, pbI18n.instance.setI18n, true))

                url = "i18n/" + $.cookie("consumer_name") + "_" + language + ".json?v=[cs_version]"
                promises.push(pbI18n.instance.loadLanguageFile(url, pbI18n.instance.setI18n, true))
                Promise.all(promises).then((values) => {
                    fireEvent("i18nLanguageLoaded", {"language_id": language})
                })
            })
        })
    }

    setI18n = function (response) {
        let promise = new Promise(function (resolve, reject) {
            try {
                let data = JSON.parse(response)
                pbI18n.instance.i18n = [pbI18n.instance.i18n, data].reduce(function (r, o) {
                    Object.keys(o).forEach(function (k) {
                        r[k] = o[k];
                    });
                    return r;
                }, {});
                resolve()
            } catch (err) {
                resolve()
            }
        })
        return promise
    }

    loadLanguageFile = function (url, callback, ignoreErrors) {
        let promise = new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            xhr.ignoreErrors = ignoreErrors

            xhr.addEventListener("readystatechange", function () {
                if (this.readyState === 4) {
                    if (this.status == 200 || this.status == 204 || ignoreErrors) {
                        callback(this.responseText);
                        resolve()
                    } else {
                        resolve()
                        //alert("Beim senden der Anfrage ist etwas schief gelaufen. Bitte prüfen Sie ob der Vorgang erfolgreich war und versuchen Sie es ggf. erneut.");
                    }
                }
            });
            if (!url.includes("_undefined")) {
                xhr.open("GET", url);
                xhr.setRequestHeader("apikey", getCookie("apikey"));
                xhr.send();
            } else {
                resolve()
            }

        })
        return promise
    }

    replaceI18n = function () {
        for (let vocab of Object.keys(pbI18n.instance.i18n)) {
            try {
                if (vocab.startsWith("#") || vocab.startsWith(".")) {
                    $(vocab).html(pbI18n.instance.translate(vocab))
                }
            } catch (err) {

            }
        }

        let data_attr_selector = "*[data-i18n]"
        $(data_attr_selector).each((i, e) => {
            $(e).html(pbI18n.instance.translate($(e).data("i18n")))
        })

        data_attr_selector = "*[placeholder]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("placeholder", pbI18n.instance.translate($(e).attr("placeholder")))
        })

        data_attr_selector = "*[data-original-title]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("data-original-title", pbI18n.instance.translate($(e).attr("data-original-title")))
        })

        data_attr_selector = "*[data-hint]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("data-hint", pbI18n.instance.translate($(e).attr("data-hint")))
        })

        data_attr_selector = "*[title]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("title", pbI18n.instance.translate($(e).attr("title")))
        })
    }
}
