import {pbMedia} from "./pb-media.js?v=[cs_version]";

export class PbQrcode {
    qrCodeRecognizedCallback = () => ""
    targetElement = undefined
    errorLevels = {
        "L": QRCode.CorrectLevel.L, //Correction Capability approx 7%
        "M": QRCode.CorrectLevel.M, //Correction Capability approx 15%
        "Q": QRCode.CorrectLevel.Q, //Correction Capability approx 25%
        "H": QRCode.CorrectLevel.H //Correction Capability approx 30%
    }
    errorLevel = "M"

    constructor(targetElement, errorLevel = "M") {
        if (PbQrcode.instance) {
            return PbQrcode.instance
        }
        this.qrcode = null
        this.targetElement = targetElement
        this.errorLevel = this.errorLevels[errorLevel]
        PbQrcode.instance = this
        return PbQrcode.instance
    }

    initGenerator() {
        PbQrcode.instance.qrcode = new QRCode(this.targetElement, {
            text: "",
            width: 256,
            height: 256,
            border: 4,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: PbQrcode.instance.errorLevel
        })
        return PbQrcode.instance;
    }

    newQrCode(contentString = "", targetElement = null) {
        if (targetElement != null) {
            this.targetElement = targetElement
            PbQrcode.instance.init();
        }
        PbQrcode.instance.qrcode.clear(); // clear the code.
        PbQrcode.instance.qrcode.makeCode(contentString); // make another code
    }

    scanVideoForQR(videoElement = document.getElementById('pbMediaVideo')) {
        QrScanner.scanImage(videoElement).then(result => { PbQrcode.instance.qrCodeRecognizedCallback(JSON.parse(result))}).catch(error => "");
    }

    initScanner(callback) {
        // https://github.com/nimiq/qr-scanner
        QrScanner.WORKER_PATH = './js/pb/vendor/qr-scanner-worker.min.js';
        PbQrcode.instance.qrCodeRecognizedCallback = callback
        setInterval(PbQrcode.instance.scanVideoForQR, 250)
        //PbQrcode.instance.qrScanner.start();

        // let scanner = new Instascan.Scanner({
        //     continuous: true,
        //     mirror: true,
        //     backgroundScan: true,
        //     refractoryPeriod: 5000,
        //     scanPeriod: 1
        // });
        // scanner.addListener('scan', function (content) {
        //     let data = JSON.parse(decodeURIComponent(escape(content)))
        //     callback(data)
        // });
        //
        // console.log("Signup FOR STREAMRUNNING")
        // document.addEventListener("gotMultimediaDevices", () => {
        //     PbQrcode.instance.usedVideoDevice = pbMedia.instance.getCurrentVideoDevice();
        //     console.log("USED DEVICE QR", pbMedia.instance.getCurrentVideoDevice());
        //     let Camera = new Instascan.Camera();
        //     Camera.id = PbQrcode.instance.usedVideoDevice.deviceId
        //     Camera.name = PbQrcode.instance.usedVideoDevice.label
        //     //scanner.start(Camera);
        // })

        // Instascan.Camera.getCameras().then(function (cameras) {
        //     console.log("CAMERAS", cameras)
        //     alert("cameras")
        //     let i = 0
        //     let items = []
        //     for(let camera of cameras) {
        //         console.log(camera)
        //         if(camera.name != null)
        //             items.push({name: camera.name, value: i})
        //         i++;
        //     }
        //     console.log("ITEMS", items)
        //     // $('#camera-selection-wrapper').dropdown({
        //     //     values: items
        //     // });
        //
        //     $("#camera_index").change(function() {
        //         scanner.start(cameras[$("#camera_index").val()]);
        //     })
        //
        //     if(!isFirefox) {
        //         $("#camera-selection-wrapper").show()
        //     }
        //
        //     if (cameras.length > 0) {
        //         if(cameras[0].name != null) {
        //             $("#camera-selection-default-text").html(cameras[0].name)
        //         }
        //
        //         scanner.start(cameras[0]);
        //     } else {
        //         console.error('No cameras found.');
        //     }
        // }).catch(function (e) {
        //     console.error(e);
        // });
    }
}